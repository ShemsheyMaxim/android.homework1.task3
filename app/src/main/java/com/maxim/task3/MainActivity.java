package com.maxim.task3;

import android.content.Context;
import android.inputmethodservice.InputMethodService;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    EditText login, email, mobileNumber, createaPassword, confirmYourPassword;
    TextInputLayout loginLayout, emailLayout, mobileNumberLayout, createaPasswordLayout, confirmYourPasswordLayout;
    Button createAccount;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = (EditText) findViewById(R.id.editText_login);
        email = (EditText) findViewById(R.id.editText_email);
        mobileNumber = (EditText) findViewById(R.id.editText_mobile_number);
        createaPassword = (EditText) findViewById(R.id.editText_create_a_password);
        confirmYourPassword = (EditText) findViewById(R.id.editText_confirm_your_password);

        loginLayout = (TextInputLayout) findViewById(R.id.login_layout);
        emailLayout = (TextInputLayout) findViewById(R.id.email_layout);
        mobileNumberLayout = (TextInputLayout) findViewById(R.id.mobile_number_layout);
        createaPasswordLayout = (TextInputLayout) findViewById(R.id.create_a_password_layout);
        confirmYourPasswordLayout = (TextInputLayout) findViewById(R.id.confirm_your_password_layout);

//        login.setOnFocusChangeListener(this);
//        email.setOnFocusChangeListener(this);
//        mobileNumber.setOnFocusChangeListener(this);
//        createaPassword.setOnFocusChangeListener(this);
//        confirmYourPassword.setOnFocusChangeListener(this);

        login.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (login.getText().toString().isEmpty()) {
                    loginLayout.setErrorEnabled(true);
                    loginLayout.setError(getResources().getString(R.string.login_empty));
                } else {
                    loginLayout.setErrorEnabled(false);
                }
                emailLayout.setErrorEnabled(false);
                mobileNumberLayout.setErrorEnabled(false);
                createaPasswordLayout.setErrorEnabled(false);
                confirmYourPasswordLayout.setErrorEnabled(false);
            }
        });

        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if ( email.getText().toString().isEmpty()) {
                    emailLayout.setErrorEnabled(true);
                    emailLayout.setError(getResources().getString(R.string.email_empty));
                } else {
                    emailLayout.setErrorEnabled(false);
                }
                loginLayout.setErrorEnabled(false);
                mobileNumberLayout.setErrorEnabled(false);
                createaPasswordLayout.setErrorEnabled(false);
                confirmYourPasswordLayout.setErrorEnabled(false);
            }
        });

        mobileNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if ( mobileNumber.getText().toString().isEmpty()) {
                    mobileNumberLayout.setErrorEnabled(true);
                    mobileNumberLayout.setError(getResources().getString(R.string.mobile_number_empty));
                } else {
                    mobileNumberLayout.setErrorEnabled(false);
                }
                loginLayout.setErrorEnabled(false);
                emailLayout.setErrorEnabled(false);
                createaPasswordLayout.setErrorEnabled(false);
                confirmYourPasswordLayout.setErrorEnabled(false);
            }
        });

        createaPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (createaPassword.getText().toString().isEmpty()) {
                    createaPasswordLayout.setErrorEnabled(true);
                    createaPasswordLayout.setError(getResources().getString(R.string.create_a_password_empty));
                } else {
                    createaPasswordLayout.setErrorEnabled(false);
                }
                loginLayout.setErrorEnabled(false);
                emailLayout.setErrorEnabled(false);
                mobileNumberLayout.setErrorEnabled(false);
                confirmYourPasswordLayout.setErrorEnabled(false);
            }
        });

        confirmYourPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (confirmYourPassword.getText().toString().isEmpty()) {
                    confirmYourPasswordLayout.setErrorEnabled(true);
                    confirmYourPasswordLayout.setError(getResources().getString(R.string.confirm_your_password_empty));
                } else {
                    confirmYourPasswordLayout.setErrorEnabled(false);
                }
                loginLayout.setErrorEnabled(false);
                emailLayout.setErrorEnabled(false);
                mobileNumberLayout.setErrorEnabled(false);
                createaPasswordLayout.setErrorEnabled(false);
            }
        });


        result = (TextView) findViewById(R.id.textView_result);

        createAccount = (Button) findViewById(R.id.button_create_account);

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();

                String login = loginLayout.getEditText().getText().toString();
                String email = emailLayout.getEditText().getText().toString();
                String mobileNumber = mobileNumberLayout.getEditText().getText().toString();
                String createaPassword = createaPasswordLayout.getEditText().getText().toString();
                String confirmYourPassword = confirmYourPasswordLayout.getEditText().getText().toString();

                if (!validateLogin(login)) {
                    loginLayout.setError("Not a valid login!");
                } else if (!validateEmail(email)) {
                    emailLayout.setError("Not a valid email address!");
                } else if (!validateMobileNumber(mobileNumber)) {
                    mobileNumberLayout.setError("Not a valid mobile number!");
                } else if (!validateCreateaPassword(createaPassword)) {
                    createaPasswordLayout.setError("Not a valid password!");
                }
                else if (!validateConfirmYourPassword(confirmYourPassword)){
                    confirmYourPasswordLayout.setError("Not a valid confirm password! Passwords do not match");
                }
                else {
                    loginLayout.setErrorEnabled(false);
                    emailLayout.setErrorEnabled(false);
                    mobileNumberLayout.setErrorEnabled(false);
                    createaPasswordLayout.setErrorEnabled(false);
//                    confirmYourPasswordLayout.setErrorEnabled(false);
                    result.setText("OK! I'm performing login.");
                }
            }
        });
    }

//    @Override
//    public void onFocusChange(View v, boolean hasFocus) {
//        if (v != login && login.getText().toString().isEmpty()) {
//            loginLayout.setErrorEnabled(true);
//            loginLayout.setError(getResources().getString(R.string.login_empty));
//        } else if (v != email && email.getText().toString().isEmpty()) {
//            emailLayout.setErrorEnabled(true);
//            emailLayout.setError(getResources().getString(R.string.email_empty));
//        } else if (v != mobileNumber && mobileNumber.getText().toString().isEmpty()) {
//            mobileNumberLayout.setErrorEnabled(true);
//            mobileNumberLayout.setError(getResources().getString(R.string.mobile_number_empty));
//        } else if (v != createaPassword && createaPassword.getText().toString().isEmpty()) {
//            createaPasswordLayout.setErrorEnabled(true);
//            createaPasswordLayout.setError(getResources().getString(R.string.create_a_password_empty));
//        } else if (v != confirmYourPassword && confirmYourPassword.getText().toString().isEmpty()) {
//            confirmYourPasswordLayout.setErrorEnabled(true);
//            confirmYourPasswordLayout.setError(getResources().getString(R.string.confirm_your_password_empty));
//        } else {
//            loginLayout.setErrorEnabled(false);
//            emailLayout.setErrorEnabled(false);
//            mobileNumberLayout.setErrorEnabled(false);
//            createaPasswordLayout.setErrorEnabled(false);
//            confirmYourPasswordLayout.setErrorEnabled(false);
//        }
//    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public boolean validateLogin(String login) {
        return login.length() > 5;
    }

    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
    //    private Pattern pattern;
    private Matcher matcher;
    private Pattern pattern = Pattern.compile(EMAIL_PATTERN);

    public boolean validateEmail(String email) {
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean validateMobileNumber(String mobileNumber) {
        return mobileNumber.length() == 12;
    }

    public boolean validateCreateaPassword(String createaPassword) {
        return createaPassword.length() > 5;
    }

    public boolean validateConfirmYourPassword(String confirmYourPassword) {
        return confirmYourPassword.equals(createaPassword.getText().toString());
    }
}
